function [DM, dDM, ljprimetheta, scaledccwljprimetheta, scaledccw] = lrhs_coupled_nonlinearcoll_constants(tau1, tau2, M)
% Constant quantities for constructing the
% right-hand side for the ODE discretising the coupled RE/DDE
%   x(t) = y(t) * \int_{-tau2}^{-tau1} f1(x(t+theta)) d theta
%   y'(t) = g(y(t)) + y(t) * \int_{-tau2}^{-tau1} f2(x(t+theta)) d theta
% collocated and linearised around a solution sol.
%
% See also LRHS_COUPLED_NONLINEARCOLL.
%
% lrhs_coupled_nonlinearcoll_constants.m, version 1.0, 2023-10-13
% Copyright (c) 2023 Davide Liessi
% This file is distributed under the terms of the MIT license (see `LICENSE.txt`).

% M >= 1
[x, bw, D] = cheb(M); % Chebyshev extrema in [-1, 1] and related quantities
% x, bw are column vectors
xx = (x-1)/2*tau2; % Chebyshev extrema in [-tau2, 0]
% xx is a column vector
DD = D*2/tau2; % differentiation matrix in [-tau2, 0]
% no need to rescale the barycentric weights: factors cancel out in formula
DM = DD(2:end, 2:end); % diff. mat. in [-tau2, 0] without 1st row & column
dDM = DD(2:end, :); % diff. mat. in [-tau2, 0] without 1st row

theta = (x-1)/2*(tau2-tau1)-tau1; % Chebyshev extrema in [-tau2, -tau1]
% theta is a column vector
ccw = clencurt(M);
% ccw is a row vector: make it column
ccw = ccw(:);
% Clenshaw-Curtis weights need to be rescaled by a factor of (tau2-tau1)/2,
% see below.

ljprimetheta = NaN(M+1, M); % row: index in theta; column: j
scaledccwljprimetheta = NaN(M+1, M); % row: index in theta; column: j
for j = 1:M
    % Derivatives of Lagrange polynomials for Chebyshev extrema in
    % [-tau2, 0]...
    % @(t) barint(xx, bw, DD(:,j+1), t)
    % computed at theta,
    ljprimetheta(:, j) = barint(xx, bw, DD(:,j+1), theta);
    % multiplied elementwise by ccw...
    scaledccwljprimetheta(:, j) = ccw .* ljprimetheta(:, j);
end
% ...and rescaled by (tau2-tau1)/2.
scaledccwljprimetheta = (tau2-tau1)/2 * scaledccwljprimetheta;
% To compute the integrals \int_{-tau2}^{-tau1}C(t,theta)l_j'(theta)dtheta
% as a row vector indexed by j, multiply (row-by-column) this matrix to the
% right of the row vector of the values C(t,theta).

% Scaled Clenshaw-Curtis weights are needed on their own for computing
% \int_{-tau2}^{-tau1}C(t,theta)dtheta by multiplying (row-by-column) the
% row vector of values of C(t,theta) by the next vector.
scaledccw = (tau2-tau1)/2 * ccw;

end
