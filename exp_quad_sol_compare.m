%% Compare the solutions of the quadratic RE computed with TDQRE and PSREINTEGR

% Test equation:
% y(t) = gamma/2*\int_{-3}^{-1}y(t+theta)*(1-y(t+theta))d\theta

% Copyright (c) 2023 Davide Liessi
% This file is distributed under the terms of the MIT license (see `LICENSE.txt`).

%% Exact solution

gamma = 4;
solquad = @(t) 1/2+pi/4/gamma+sqrt(1/2-1/gamma-pi/2/gamma^2*(1+pi/4))*sin(pi/2*t);

T = 500;

Mmax = 60;
rmax3 = 20;
RelTol = 1e-6;
AbsTol = 1e-7;

%% Compute solution with trapezoidal formula and measure error

err_tdqre_end = NaN(1, rmax3);
err_tdqre_max = NaN(1, rmax3);
for i = 1:rmax3
    fprintf('%d, ', i)
    [Y, t] = tdqre(1, 3, @(theta)theta*0+gamma/2, @(y) y.*(1-y), solquad, 3*i, T);
    err_tdqre_end(i) = abs(Y(end)-solquad(t(end)));
    err_tdqre_max(i) = max(abs(Y-solquad(t)));
end
fprintf('\n')

% should be order 2

%% Compute solution with PS collocation and measure error

hmin = min(diff(t));

err_ps_end = NaN(1, Mmax);
err_ps_max = NaN(1, Mmax);
for i = 1:Mmax
    fprintf('%d, ', i)
    [sol, G] = psreintegr(1, 3, @(theta)theta*0+gamma/2, @(y) y.*(1-y),solquad, i, T, RelTol, AbsTol);
    err_ps_end(i) = abs(G(deval(sol,T))-solquad(T));
    err_ps_max(i) = max(abs(G(deval(sol,0:hmin:T))-solquad(0:hmin:T)));
end
fprintf('\n')

%% Figure

figure
loglog(3*(1:rmax3), err_tdqre_end, '.-', 'DisplayName', sprintf('error at T=%d with TDQ', t(end)))
hold on
loglog(3*(1:rmax3), err_tdqre_max, '.-', 'DisplayName', sprintf('max error on [0, %d] with TDQ along solution', t(end)))
loglog(1:Mmax, err_ps_end, '.-', 'DisplayName', sprintf('error at T=%d with PS', T))
loglog(1:Mmax, err_ps_max, '.-', 'DisplayName', sprintf('max error on [0, %d] with PS with step %d', T, hmin))
legend('Location', 'best')
xlabel('M=3r')
title(sprintf('Absolute errors on solution of quadratic RE with \\gamma=%d w.r.t. exact periodic solution', gamma))
