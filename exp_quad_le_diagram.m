%% Diagram of Lyapunov exponents for the quadratic RE varying gamma

% equation is y(t) = gamma/2*\int_{-3}^{-1}y(t+theta)*(1-y(t+theta))d\theta
% trivial equilibrium 0 is stable for 0 < gamma < 1
% nontrivial equilibrium 1-1/gamma is stable for 1 < gamma < 2+pi/2
% periodic solution is stable for 2+pi/2 < gamma <\approx 4.32
% C(t,theta) = gamma/2*(1-2*\bar{y})

% Copyright (c) 2023 Davide Liessi
% This file is distributed under the terms of the MIT license (see `LICENSE.txt`).

%% Parameters

tau1 = 1;
tau2 = 3;

gamma_start = 3;
gamma_end = 5;
delta_gamma = 0.01;

M = 15;
T = 1000;

TOL = 1e-6; % dqr
RelTol = 1e-6; % ode45
AbsTol = 1e-7; % ode45

% 1st branch of exact periodic solutions
solquad = @(t,gamma) 1/2+pi/4/gamma+sqrt(1/2-1/gamma-pi/2/gamma^2*(1+pi/4))*sin(pi/2*t);

gamma = gamma_start:delta_gamma:gamma_end;
[DM, lkprimetheta, scaledccwljprimetheta] = lrhs_nonlinearcoll_constants(tau1, tau2, M);

%% Compute, CL (collocation and then linearization)

le_all = cell(length(gamma), 1);
le_t = cell(length(gamma), 1);
le_clock = cell(length(gamma), 1);
le_rejects = NaN(length(gamma), 1);

for i = 1:length(gamma)
    fprintf('gamma = %d\n', gamma(i))
    if gamma(i) <= 1
        sol = psreintegr(1, 3, @(theta)theta*0+gamma(i)/2, @(y) y.*(1-y), @(t) t*0, M, T, RelTol, AbsTol);
    elseif gamma(i) <= 2+pi/2
        sol = psreintegr(1, 3, @(theta)theta*0+gamma(i)/2, @(y) y.*(1-y), @(t) t*0+1-1/gamma(i), M, T, RelTol, AbsTol);
    elseif gamma(i) <= 4.3
        sol = psreintegr(1, 3, @(theta)theta*0+gamma(i)/2, @(y) y.*(1-y), @(t)solquad(t,gamma(i)), M, T, RelTol, AbsTol);
    else
        if gamma(i) <= 4.67
            initial_value = 0.5;
        else
            initial_value = 0.2;
        end
        sol = psreintegr(1, 3, @(theta)theta*0+gamma(i)/2, @(y) y.*(1-y), @(t) t*0+initial_value, M, T, RelTol, AbsTol);
    end
    A = @(t) lrhs_nonlinearcoll(t, @(y) gamma(i)/2 * (1 - 2*y), sol, M, DM, lkprimetheta, scaledccwljprimetheta);
    % always reset the random number generator
    rng('default')
    [le_all{i}, le_t{i}, le_clock{i}, le_rejects(i)] = dqr(A, T, TOL);
end

%% Plot

% Due to the large amount of data it is better to split the variables in
% several MAT files: e.g. for each block of 100 values of gamma, start from
% clean variables, compute and save to a different MAT file.

le = cellfun(@(x) x(:,end), le_all, 'UniformOutput', false);

figure
plot(gamma, cellfun(@(x) x(1), le), '.-', 'DisplayName', 'LE(1)')
hold on
plot(gamma, cellfun(@(x) x(2), le), '.-', 'DisplayName', 'LE(2)')
grid on
title(sprintf('LE(1:2) for quadratic RE with T=%d, M=%d, TOL=%d', T, M, TOL))
xlabel('\gamma')
legend('Location', 'best')
