%% Diagram of Lyapunov exponents for the simplified logistic Daphnia varying beta

% equation is
% x(t) = beta*y(t)*\int_{-amax}^{-abar}x(t+theta)d\theta
% y'(t) = r*y(t)*(1-y(t)/K) - gamma*y(t)*\int_{-amax}^{-abar}x(t+theta)d\theta
% use beta as continuation parameter
% equilibrium (0, 1) is stable for 0 < beta < 1
% nontrivial equilibrium is stable for 1 < beta <\approx 3.0161 [Hopf, BDGSV2016]

% Copyright (c) 2023 Davide Liessi
% This file is distributed under the terms of the MIT license (see `LICENSE.txt`).

%% Parameters

abar = 3;
amax = 4;
r = 1;
K = 1;
g = @(y) r * y .* (1-y/K);
f1 = @(x) x;
gamma = 1;
k2 = @(theta) theta*0 - gamma;
f2 = @(x) x;
u = @(t) t*0+0.5;
v = @(t) t*0+0.5;

beta_start = 0;
beta_end = 8.5;
delta_beta = 0.01;

M = 15;
T = 1000;

TOL = 1e-6; % dqr
RelTol = 1e-4; % ode45
AbsTol = 1e-5; % ode45

beta = beta_start:delta_beta:beta_end;
[DM, dDM, lkprimetheta, scaledccwljprimetheta, scaledccw] = lrhs_coupled_nonlinearcoll_constants(abar, amax, M);

%% Compute, CL (collocation and then linearization)

le_all = cell(length(beta), 1);
le_t = cell(length(beta), 1);
le_clock = cell(length(beta), 1);
le_rejects = NaN(length(beta), 1);

for i = 1:length(beta)
    fprintf('beta = %d\n', beta(i))
    k1 = @(theta) theta*0 + beta(i);
    sol = psdeintegr(abar, amax, g, k1, f1, k2, f2, u, v, M, T, RelTol, AbsTol);
    A = @(t) lrhs_coupled_nonlinearcoll(t, @(x) beta(i)*x, @(x) -gamma*x, @(x) x*0+beta(i), @(x) x*0-gamma, @(y) r*(1-2*y/K), sol, M, DM, dDM, lkprimetheta, scaledccwljprimetheta, scaledccw);
    % always reset the random number generator
    rng('default')
    [le_all{i}, le_t{i}, le_clock{i}, le_rejects(i)] = dqr(A, T, TOL);
end

%% Plot

% Due to the large amount of data it is better to split the variables in
% several MAT files: e.g. for each block of 100 values of gamma, start from
% clean variables, compute and save to a different MAT file.

le = cellfun(@(x) x(:,end), le_all, 'UniformOutput', false);

figure
plot(beta, cellfun(@(x) x(1), le), '.-', 'DisplayName', 'LE(1)')
hold on
plot(beta, cellfun(@(x) x(2), le), '.-', 'DisplayName', 'LE(2)')
grid on
title(sprintf('LE(1:2) for simplified logistic Daphnia coupled RE/DDE with T=%d, M=%d, TOL=%d', T, M, TOL))
xlabel('\beta')
legend('Location', 'best')
