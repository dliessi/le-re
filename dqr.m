function [lambda,t,clock,rejects]=dqr(A,T,TOL)
%DQR Discrete QR method for Lyapunov exponents of linear ODEs.
%   [lambda,t]=dqr(A,T,TOL) computes the Lyapunov exponents of the
%   linear ODE x'(t)=A(t)x(t) with the discrete QR method according
%   to [1].
%   INPUT:
%                 A = matrix function (inline)
%                 T = truncation time
%               TOL = tolerance for error control
%   OUTPUT:
%       lambda(:,i) = exponents at the i-th time step
%                 t = sequence of time steps
%   CALL:
%       >>A=@(t)matrix expression of t;
%       >>[lambda,t]=dqr(A,1e3,1e-4)
%   REFERENCES:
%       [1] L. Dieci, M.S. Jolly and E. Van Vleck, "Numerical
%           techniques for approximating Lyapunov exponents and
%           their implementation", J. Comput. Nonlinear Dynam.
%           6(1):011003-1-7, 2010.

%DOPRI54
s=7;
Ark=[0,0,0,0,0,0,0;
    1/5,0,0,0,0,0,0;
    3/40,9/40,0,0,0,0,0;
    44/45,-56/15,32/9,0,0,0,0;
    19372/6561,-25360/2187,64448/6561,-212/729,0,0,0;
    9017/3168,-355/33,46732/5247,49/176,-5103/18656,0,0;
    35/384,0,500/1113,125/192,-2187/6784,11/84,0];
brk=[5179/57600,0,7571/16695,393/640,-92097/339200,187/2100,1/40];
crk=[0;1/5;3/10;4/5;8/9;1;1];
brkhat=Ark(s,1:s-1);
%inizialization
n=size(A(0),1);
I=eye(n);
t=0;
[Q,R]=qr(rand(n));
Q=Q*diag(diag(R)./abs(diag(R)));
h=.01;
lambda(:,1)=zeros(n,1);
rejects=0;
steps=0;
clock=cputime;
while t(end)+h<=T
    %IVP
    PSI(1:n,:)=Q;
    F(1:n,:)=A(t(end)+crk(1)*h)*Q;
    for i=2:s
        PSI(n*(i-1)+1:n*i,:)=Q+h*kron(Ark(i,1:i-1),I)*...
            F(1:n*(i-1),:);
        F(n*(i-1)+1:n*i,:)=A(t(end)+crk(i)*h)*PSI(n*(i-1)+1:n*i,:);
    end
    Psi=Q+h*kron(brk,I)*F;
    Psihat=Q+h*kron(brkhat,I)*F(1:n*(s-1),:);
    %QR
    [Qnew,R]=qr(Psi);
    Qnew=Qnew*diag(diag(R)./abs(diag(R)));
    R=abs(diag(R));
    [~,Rhat]=qr(Psihat);
    Rhat=abs(diag(Rhat));
    %error
    e=max(abs(R-Rhat)./((1+R).*TOL));
    hnew=.8*h*(1/e^.2);
    %update
    if e<=1 %accept step
        t=[t,t(end)+h];
        h=min(hnew,5*h);
        Q=Qnew;
        lambda=[lambda,(t(end-1)*lambda(:,end)+log(R))/t(end)];
        %avoid accumulation of lambda and t if not
        %interested in monitoring the time behavior
        steps=steps+1;
        clock=[clock,cputime-clock(1)];
    else h=max(hnew,h/5); %reject step
        rejects=rejects+1;
    end
end
end
