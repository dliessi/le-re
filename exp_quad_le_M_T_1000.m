%% Lyapunov exponents of the quadratic RE varying M and T

% The script expects eigTMNpw from http://cdlab.uniud.it/software to be
% available in ~/src/eigtmnpw.

% equation is y(t) = gamma/2*\int_{-3}^{-1}y(t+theta)*(1-y(t+theta))d\theta
% trivial equilibrium 0 is stable for 0 < gamma < 1
% nontrivial equilibrium 1-1/gamma is stable for 1 < gamma < 2+pi/2
% periodic solution is stable for 2+pi/2 < gamma <\approx 4.32
% C(t,theta) = gamma/2*(1-2*\bar{y})

% Copyright (c) 2023 Davide Liessi
% This file is distributed under the terms of the MIT license (see `LICENSE.txt`).

%% Parameters

M = [8:2:20, 25, 30];
Mref = 120;
T = 1000;
TOL = 1e-6;
tau1 = 1;
tau2 = 3;

gamma_triveq = 0.5;
gamma_nontriveq = 3;
gamma_perexact = 4;
solquad = @(t) 1/2+pi/4/gamma_perexact+sqrt(1/2-1/gamma_perexact-pi/2/gamma_perexact^2*(1+pi/4))*sin(pi/2*t);

RelTol = 1e-6;
AbsTol = 1e-7;

%% Reference values: Floquet multipliers from eigTMNpw

mydir = pwd;
cd ~/src/eigtmnpw

tmnpw_m = eigTMNpw_method(Mref);
tmnpw_s_triveq = eigTMNpw_system([1 0], [1 3], 3, 0, ...
    'CXX', { ...
    @(t, theta, par) 0, ...
    @(t, theta, par) par/2 ...
    }, ...
    'par', gamma_triveq);
tmnpw_s_nontriveq = eigTMNpw_system([1 0], [1 3], 3, 0, ...
    'CXX', { ...
    @(t, theta, par) 0, ...
    @(t, theta, par) 1-par/2 ...
    }, ...
    'par', gamma_nontriveq);
tmnpw_s_perexact = eigTMNpw_system([1 0], [1 3], 4, 0, ...
    'CXX', { ...
    @(t, theta, par) 0, ...
    @(t, theta, par) par/2*(1-2*solquad(t+theta)) ...
    }, ...
    'par', gamma_perexact);
multpw_triveq = eigTMNpw(tmnpw_s_triveq, tmnpw_m);
multpw_nontriveq = eigTMNpw(tmnpw_s_nontriveq, tmnpw_m);
multpw_perexact = eigTMNpw(tmnpw_s_perexact, tmnpw_m);

cd(mydir)

%% Trivial equilibrium, LC (linearization and then collocation)

le_triveq_lc_all = cell(length(M), 1);
le_triveq_lc_t = cell(length(M), 1);
le_triveq_lc_clock = cell(length(M), 1);
le_triveq_lc_rejects = NaN(length(M), 1);
for i = 1:length(M)
    fprintf('\ntrivial eq., LC, M=%d, T=%d, TOL=%d\n', M(i), T, TOL)
    [DM, theta, scaledccwljprimetheta] = lrhs_linear2_constants(tau1, tau2, M(i));
    A = @(t) lrhs_linear2(t, @(tt,theta)theta*0+gamma_triveq/2, M(i), DM, theta, scaledccwljprimetheta);
    % always reset the random number generator
    rng('default')
    [le_triveq_lc_all{i}, le_triveq_lc_t{i}, le_triveq_lc_clock{i}, le_triveq_lc_rejects(i)] = dqr(A, T, TOL);
end

%% Nontrivial equilibrium, LC (linearization and then collocation)

le_nontriveq_lc_all = cell(length(M), 1);
le_nontriveq_lc_t = cell(length(M), 1);
le_nontriveq_lc_clock = cell(length(M), 1);
le_nontriveq_lc_rejects = NaN(length(M), 1);
for i = 1:length(M)
    fprintf('\nnontrivial eq., LC, M=%d, T=%d, TOL=%d\n', M(i), T, TOL)
    [DM, theta, scaledccwljprimetheta] = lrhs_linear2_constants(tau1, tau2, M(i));
    A = @(t) lrhs_linear2(t, @(tt,theta)(theta*0+1-gamma_nontriveq/2), M(i), DM, theta, scaledccwljprimetheta);
    % always reset the random number generator
    rng('default')
    [le_nontriveq_lc_all{i}, le_nontriveq_lc_t{i}, le_nontriveq_lc_clock{i}, le_nontriveq_lc_rejects(i)] = dqr(A, T, TOL);
end

%% Exact periodic solution, LC (linearization and then collocation)

le_perexact_lc_all = cell(length(M), 1);
le_perexact_lc_t = cell(length(M), 1);
le_perexact_lc_clock = cell(length(M), 1);
le_perexact_lc_rejects = NaN(length(M), 1);
for i = 1:length(M)
    fprintf('\nexact per. sol., LC, M=%d, T=%d, TOL=%d\n', M(i), T, TOL)
    [DM, theta, scaledccwljprimetheta] = lrhs_linear2_constants(tau1, tau2, M(i));
    A = @(t) lrhs_linear2(t, @(tt,theta)gamma_perexact/2*(1-2*solquad(tt+theta)), M(i), DM, theta, scaledccwljprimetheta);
    % always reset the random number generator
    rng('default')
    [le_perexact_lc_all{i}, le_perexact_lc_t{i}, le_perexact_lc_clock{i}, le_perexact_lc_rejects(i)] = dqr(A, T, TOL);
end

%% Numerical periodic solution, LC (linearization and then collocation)

r = 6:3:30;
le_pernum_lc_all = cell(length(r), 1);
le_pernum_lc_t = cell(length(r), 1);
le_pernum_lc_clock = cell(length(r), 1);
le_pernum_lc_rejects = NaN(length(r), 1);
for i = 1:length(r)
    fprintf('\nnum. per. sol., LC, r=%d, T=%d, TOL=%d\n', r(i), T, TOL)
    [DM, theta, scaledccwljprimetheta] = lrhs_linear2_constants(tau1, tau2, r(i));
    [Y, tY, U, tU] = tdqre(1, 3, @(theta)theta*0+gamma_perexact/2, @(y) y.*(1-y), solquad, r(i), T);
    solnumtdq = @(t) interp1d(Y, tY, U(end:-1:1), tU(end:-1:1), t);
    A = @(t) lrhs_linear2(t, @(tt,theta)gamma_perexact/2*(1-2*solnumtdq(tt+theta)), r(i), DM, theta, scaledccwljprimetheta);
    % always reset the random number generator
    rng('default')
    [le_pernum_lc_all{i}, le_pernum_lc_t{i}, le_pernum_lc_clock{i}, le_pernum_lc_rejects(i)] = dqr(A, T, TOL);
end

%% Trivial equilibrium, CL (collocation and then linearization)

le_triveq_cl_all = cell(length(M), 1);
le_triveq_cl_t = cell(length(M), 1);
le_triveq_cl_clock = cell(length(M), 1);
le_triveq_cl_rejects = NaN(length(M), 1);
for i = 1:length(M)
    fprintf('\ntrivial eq., CL, M=%d, T=%d, TOL=%d\n', M(i), T, TOL)
    [sol, G] = psreintegr(1, 3, @(theta)theta*0+gamma_triveq/2, @(y) y.*(1-y), @(t) t*0, M(i), T, RelTol, AbsTol);
    [DM, lkprimetheta, scaledccwljprimetheta] = lrhs_nonlinearcoll_constants(tau1, tau2, M(i));
    A = @(t) lrhs_nonlinearcoll(t, @(y) gamma_triveq/2 * (1 - 2*y), sol, M(i), DM, lkprimetheta, scaledccwljprimetheta);
    % always reset the random number generator
    rng('default')
    [le_triveq_cl_all{i}, le_triveq_cl_t{i}, le_triveq_cl_clock{i}, le_triveq_cl_rejects(i)] = dqr(A, T, TOL);
end

%% Nontrivial equilibrium, CL (collocation and then linearization)

le_nontriveq_cl_all = cell(length(M), 1);
le_nontriveq_cl_t = cell(length(M), 1);
le_nontriveq_cl_clock = cell(length(M), 1);
le_nontriveq_cl_rejects = NaN(length(M), 1);
for i = 1:length(M)
    fprintf('\nnontrivial eq., CL, M=%d, T=%d, TOL=%d\n', M(i), T, TOL)
    [sol, G] = psreintegr(1, 3, @(theta)theta*0+gamma_nontriveq/2, @(y) y.*(1-y), @(t) t*0+1-1/gamma_nontriveq, M(i), T, RelTol, AbsTol);
    [DM, lkprimetheta, scaledccwljprimetheta] = lrhs_nonlinearcoll_constants(tau1, tau2, M(i));
    A = @(t) lrhs_nonlinearcoll(t, @(y) gamma_nontriveq/2 * (1 - 2*y), sol, M(i), DM, lkprimetheta, scaledccwljprimetheta);
    % always reset the random number generator
    rng('default')
    [le_nontriveq_cl_all{i}, le_nontriveq_cl_t{i}, le_nontriveq_cl_clock{i}, le_nontriveq_cl_rejects(i)] = dqr(A, T, TOL);
end

%% Exact periodic solution, CL (collocation and then linearization)

le_perexact_cl_all = cell(length(M), 1);
le_perexact_cl_t = cell(length(M), 1);
le_perexact_cl_clock = cell(length(M), 1);
le_perexact_cl_rejects = NaN(length(M), 1);
for i = 1:length(M)
    fprintf('\nexact per. sol., CL, M=%d, T=%d, TOL=%d\n', M(i), T, TOL)
    [sol, G] = psreintegr(1, 3, @(theta)theta*0+gamma_perexact/2, @(y) y.*(1-y), solquad, M(i), T, RelTol, AbsTol);
    [DM, lkprimetheta, scaledccwljprimetheta] = lrhs_nonlinearcoll_constants(tau1, tau2, M(i));
    A = @(t) lrhs_nonlinearcoll(t, @(y) gamma_perexact/2 * (1 - 2*y), sol, M(i), DM, lkprimetheta, scaledccwljprimetheta);
    % always reset the random number generator
    rng('default')
    [le_perexact_cl_all{i}, le_perexact_cl_t{i}, le_perexact_cl_clock{i}, le_perexact_cl_rejects(i)] = dqr(A, T, TOL);
end

%% LE(M)

err_triveq_lc = NaN(length(M), 1);
err_triveq_cl = NaN(length(M), 1);
err_nontriveq_lc = NaN(length(M), 1);
err_nontriveq_cl = NaN(length(M), 1);
err_perexact_lc_1 = NaN(length(M), 1);
err_perexact_lc_2 = NaN(length(M), 1);
err_pernum_lc_1 = NaN(length(r), 1);
err_pernum_lc_2 = NaN(length(r), 1);
err_perexact_cl_1 = NaN(length(M), 1);
err_perexact_cl_2 = NaN(length(M), 1);
for i = 1:length(M)
    err_triveq_lc(i) = abs(le_triveq_lc_all{i}(1,end)-real(log(multpw_triveq(1))/3));
    err_nontriveq_lc(i) = abs(le_nontriveq_lc_all{i}(1,end)-real(log(multpw_nontriveq(1))/3));
    err_perexact_lc_1(i) = abs(le_perexact_lc_all{i}(1,end)-real(log(multpw_perexact(1))/4));
    err_perexact_lc_2(i) = abs(le_perexact_lc_all{i}(2,end)-real(log(multpw_perexact(2))/4));
    err_triveq_cl(i) = abs(le_triveq_cl_all{i}(1,end)-real(log(multpw_triveq(1))/3));
    err_nontriveq_cl(i) = abs(le_nontriveq_cl_all{i}(1,end)-real(log(multpw_nontriveq(1))/3));
    err_perexact_cl_1(i) = abs(le_perexact_cl_all{i}(1,end)-real(log(multpw_perexact(1))/4));
    err_perexact_cl_2(i) = abs(le_perexact_cl_all{i}(2,end)-real(log(multpw_perexact(2))/4));
end
for i = 1:length(r)
    err_pernum_lc_1(i) = abs(le_pernum_lc_all{i}(1,end)-real(log(multpw_perexact(1))/4));
    err_pernum_lc_2(i) = abs(le_pernum_lc_all{i}(2,end)-real(log(multpw_perexact(2))/4));
end

figure
loglog(M, err_triveq_lc, '.-', 'DisplayName', sprintf('trivial eq., \\gamma=%d, T\\approx%d, 1st LE, LC', gamma_triveq, T));
hold on
loglog(M, err_triveq_cl, 'x-', 'DisplayName', sprintf('trivial eq., \\gamma=%d, T\\approx%d, 1st LE, CL', gamma_triveq, T));
loglog(M, err_nontriveq_lc, '.-', 'DisplayName', sprintf('nontrivial eq., \\gamma=%d, T\\approx%d, 1st LE, LC', gamma_nontriveq, T));
loglog(M, err_nontriveq_cl, 'x-', 'DisplayName', sprintf('nontrivial eq., \\gamma=%d, T\\approx%d, 1st LE, CL', gamma_nontriveq, T));
loglog(M, err_perexact_lc_1, '.-', 'DisplayName', sprintf('exact per. sol., \\gamma=%d, T\\approx%d, trivial LE, LC', gamma_perexact, T));
loglog(r, err_pernum_lc_1, '.-', 'DisplayName', sprintf('num. per. sol., \\gamma=%d, T\\approx%d, trivial LE, LC', gamma_perexact, T));
loglog(M, err_perexact_cl_1, 'x-', 'DisplayName', sprintf('exact per. sol., \\gamma=%d, T\\approx%d, trivial LE, CL', gamma_perexact, T));
loglog(M, err_perexact_lc_2, '.-', 'DisplayName', sprintf('exact per. sol., \\gamma=%d, T\\approx%d, 1st nontrivial LE, LC', gamma_perexact, T));
loglog(r, err_pernum_lc_2, '.-', 'DisplayName', sprintf('num. per. sol., \\gamma=%d, T\\approx%d, 1st nontrivial LE, LC', gamma_perexact, T));
loglog(M, err_perexact_cl_2, 'x-', 'DisplayName', sprintf('exact per. sol., \\gamma=%d, T\\approx%d, 1st nontrivial LE, CL', gamma_perexact, T));
title(sprintf('abs. error on LE w.r.t. Floquet/eigTMNpw with M=%d, quadratic RE', Mref))
xlabel('M(=r)')
legend('Location', 'best')

%% LE(T)

figure
for i = 1:length(M)
    loglog(le_triveq_lc_t{i}, abs(le_triveq_lc_all{i}(1,:)-real(log(multpw_triveq(1))/3)), 'DisplayName', sprintf('M=%d, LC', M(i)));
    hold on
    loglog(le_triveq_cl_t{i}, abs(le_triveq_cl_all{i}(1,:)-real(log(multpw_triveq(1))/3)), 'DisplayName', sprintf('M=%d, CL', M(i)));
end
title(sprintf('abs. error on 1st LE of trivial eq. (\\gamma=%d) w.r.t. Floquet/eigTMNpw with M=%d, quadratic RE', gamma_triveq, Mref))
xlabel('T')
legend('Location', 'best')

figure
for i = 1:length(M)
    loglog(le_nontriveq_lc_t{i}, abs(le_nontriveq_lc_all{i}(1,:)-real(log(multpw_nontriveq(1))/3)), 'DisplayName', sprintf('M=%d, LC', M(i)));
    hold on
    loglog(le_nontriveq_cl_t{i}, abs(le_nontriveq_cl_all{i}(1,:)-real(log(multpw_nontriveq(1))/3)), 'DisplayName', sprintf('M=%d, CL', M(i)));
end
title(sprintf('abs. error on 1st LE of nontrivial eq. (\\gamma=%d) w.r.t. Floquet/eigTMNpw with M=%d, quadratic RE', gamma_nontriveq, Mref))
xlabel('T')
legend('Location', 'best')

figure
for i = 1:length(M)
    loglog(le_perexact_lc_t{i}, abs(le_perexact_lc_all{i}(1,:)-real(log(multpw_perexact(1))/4)), 'DisplayName', sprintf('M=%d, LC', M(i)));
    hold on
    loglog(le_perexact_cl_t{i}, abs(le_perexact_cl_all{i}(1,:)-real(log(multpw_perexact(1))/4)), 'DisplayName', sprintf('M=%d, CL', M(i)));
end
for i = 1:length(r)
    loglog(le_pernum_lc_t{i}, abs(le_pernum_lc_all{i}(1,:)-real(log(multpw_perexact(1))/4)), 'DisplayName', sprintf('r=%d, LCnum', r(i)));
end
title(sprintf('abs. error on trivial LE of exact/num. per. sol. (\\gamma=%d) w.r.t. Floquet/eigTMNpw with M=%d, quadratic RE', gamma_perexact, Mref))
xlabel('T')
legend('Location', 'best')

figure
for i = 1:length(M)
    loglog(le_perexact_lc_t{i}, abs(le_perexact_lc_all{i}(2,:)-real(log(multpw_perexact(2))/4)), 'DisplayName', sprintf('M=%d, LC', M(i)));
    hold on
    loglog(le_perexact_cl_t{i}, abs(le_perexact_cl_all{i}(2,:)-real(log(multpw_perexact(2))/4)), 'DisplayName', sprintf('M=%d, CL', M(i)));
end
for i = 1:length(r)
    loglog(le_pernum_lc_t{i}, abs(le_pernum_lc_all{i}(2,:)-real(log(multpw_perexact(2))/4)), 'DisplayName', sprintf('r=%d, LCnum', r(i)));
end
title(sprintf('abs. error on 1st nontrivial LE of exact/num. per. sol. (\\gamma=%d) w.r.t. Floquet/eigTMNpw with M=%d, quadratic RE', gamma_perexact, Mref))
xlabel('T')
legend('Location', 'best')

%% LE(T) filtered, only CL

npoints = 200;
Mindices = [1, 3, 5, 7];

figure

subplot(2,2,1)
for i = Mindices
    iii = unique(floor(logspace(0,log10(length(le_triveq_cl_t{i})),npoints)));
    loglog(le_triveq_cl_t{i}(iii), abs(le_triveq_cl_all{i}(1,iii)-real(log(multpw_triveq(1))/3)), 'DisplayName', sprintf('M=%d, CL', M(i)));
    hold on
end
title(sprintf('abs. error on 1st LE of trivial eq. (\\gamma=%d) w.r.t. Floquet/eigTMNpw with M=%d, quadratic RE', gamma_triveq, Mref))
xlabel('T')
legend('Location', 'best')

subplot(2,2,2)
for i = Mindices
    iii = unique(floor(logspace(0,log10(length(le_nontriveq_cl_t{i})),npoints)));
    loglog(le_nontriveq_cl_t{i}(iii), abs(le_nontriveq_cl_all{i}(1,iii)-real(log(multpw_nontriveq(1))/3)), 'DisplayName', sprintf('M=%d, CL', M(i)));
    hold on
end
title(sprintf('abs. error on 1st LE of nontrivial eq. (\\gamma=%d) w.r.t. Floquet/eigTMNpw with M=%d, quadratic RE', gamma_nontriveq, Mref))
xlabel('T')
legend('Location', 'best')

subplot(2,2,3)
for i = Mindices
    iii = unique(floor(logspace(0,log10(length(le_perexact_cl_t{i})),npoints)));
    loglog(le_perexact_cl_t{i}(iii), abs(le_perexact_cl_all{i}(1,iii)-real(log(multpw_perexact(1))/4)), 'DisplayName', sprintf('M=%d, CL', M(i)));
    hold on
end
title(sprintf('abs. error on trivial LE of exact per. sol. (\\gamma=%d) w.r.t. Floquet/eigTMNpw with M=%d, quadratic RE', gamma_perexact, Mref))
xlabel('T')
legend('Location', 'best')

subplot(2,2,4)
for i = Mindices
    iii = unique(floor(logspace(0,log10(length(le_perexact_cl_t{i})),npoints)));
    loglog(le_perexact_cl_t{i}(iii), abs(le_perexact_cl_all{i}(2,iii)-real(log(multpw_perexact(2))/4)), 'DisplayName', sprintf('M=%d, CL', M(i)));
    hold on
end
title(sprintf('abs. error on 1st nontrivial LE of exact per. sol. (\\gamma=%d) w.r.t. Floquet/eigTMNpw with M=%d, quadratic RE', gamma_perexact, Mref))
xlabel('T')
legend('Location', 'best')
