%% Tests for PSREINTEGR (time integration of RE via pseudospectral discretization)

% Test equation:
% y(t) = gamma/2*\int_{-3}^{-1}y(t+theta)*(1-y(t+theta))d\theta
% trivial equilibrium 0 is stable for 0 < gamma < 1
% nontrivial equilibrium 1-1/gamma is stable for 1 < gamma < 2+pi/2
% periodic solution is stable for 2+pi/2 < gamma <\approx 4.32

% Copyright (c) 2023 Davide Liessi
% This file is distributed under the terms of the MIT license (see `LICENSE.txt`).

%% Parameters

tau1 = 1;
tau2 = 3;
k = @(theta,gamma) theta*0+gamma/2;
g = @(y) y.*(1-y);

AbsTol = 1e-6;
RelTol = 1e-7;

%% trivial equilibrium

gamma = 0.5;
[sol, G] = psreintegr(tau1, tau2, @(theta)k(theta,gamma), g, @(t) t*0, 20, 3, RelTol, AbsTol);
figure
plot(linspace(0,3,100), G(deval(sol, linspace(0,3,100))))

%% nontrivial equilibrium

gamma = 3;
[sol, G] = psreintegr(tau1, tau2, @(theta)k(theta,gamma), g, @(t) t*0+1-1/gamma, 20, 3, RelTol, AbsTol);
figure
plot(linspace(0,3,100), G(deval(sol, linspace(0,3,100))))

%% periodic solution

gamma = 4;
% exact periodic solution
solquad = @(t) 1/2+pi/4/gamma+sqrt(1/2-1/gamma-pi/2/gamma^2*(1+pi/4))*sin(pi/2*t);

[sol, G] = psreintegr(tau1, tau2, @(theta)k(theta,gamma), g, @(t) t*0+0.5, 20, 100, RelTol, AbsTol);
figure
plot(linspace(0,100,1000), G(deval(sol, linspace(0,100,1000))))
hold on
plot(linspace(0,100,1000), solquad(linspace(0,100,1000)))

[sol, G] = psreintegr(tau1, tau2, @(theta)k(theta,gamma), g, solquad, 20, 100, RelTol, AbsTol);
figure
plot(linspace(0,100,1000), G(deval(sol, linspace(0,100,1000))))
hold on
plot(linspace(0,100,1000), solquad(linspace(0,100,1000)))

err = NaN(1,50);
for i=1:50
    fprintf('%d, ', i)
    [sol, G] = psreintegr(tau1, tau2, @(theta)k(theta,gamma), g, solquad, i, 500, RelTol, AbsTol);
    err(i) = abs(G(deval(sol,500))-solquad(500));
    %err(i) = max(abs(G(deval(sol,linspace(0,500,5000)))-solquad(linspace(0,500,5000))));
end
fprintf('\n')
figure
loglog(1:50, err)
