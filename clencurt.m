function [w, x] = clencurt(N)
%CLENCURT Weights and nodes of Clenshaw-Curtis quadrature
%  [w, x] = CLENCURT(N) returns the weights of the Clenshaw-Curtis
%  quadrature, i.e. the pseudospectral method on N+1 Chebyshev
%  nodes x in [-1, 1].
%
%  Reference:
%    L. N. Trefethen,
%    Spectral Methods in MATLAB,
%    SIAM, 2000,
%    DOI: 10.1137/1.9780898719598

theta = pi * (0:N)' / N;
x = cos(theta);
w = zeros(1, N+1);
ii = 2:N;
v = ones(N-1, 1);
if mod(N, 2) == 0
    w(1) = 1 / (N^2-1);
    w(N+1) = w(1);
    for k = 1:N/2-1
        v = v - 2 * cos(2*k*theta(ii)) / (4*k^2-1);
    end
    v = v - cos(N*theta(ii)) / (N^2-1);
else
    w(1) = 1 / N^2;
    w(N+1) = w(1);
    for	k = 1:(N-1)/2
        v = v - 2 * cos(2*k*theta(ii)) / (4*k^2-1);
    end
end
w(ii) = 2 * v / N;
end
