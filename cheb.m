function [x, w, D] = cheb(N)
%CHEB Chebyshev type II nodes (extrema) and relevant quantities
%  [x, w, D] = CHEB(N) returns the N+1 Chebyshev nodes x in [-1, 1]
%  and the corresponding barycentric interpolation weights w and
%  differentiation matrix D.
%
%  References:
%    L. N. Trefethen,
%    Spectral Methods in MATLAB,
%    SIAM, 2000,
%    DOI: 10.1137/1.9780898719598
%
%    J.-P. Berrut and L. N. Trefethen,
%    Barycentric Lagrange interpolation,
%    SIAM Review, 46(3):501-517, 2004,
%    DOI: 10.1137/S0036144502417715

assert(N>0)
x = cos(pi*(0:N)/N)';
c = [2; ones(N-1,1); 2].*(-1).^(0:N)';
X = repmat(x,1,N+1);
dX = X-X';
D = (c*(1./c)')./(dX+(eye(N+1)));
D = D-diag(sum(D,2));

w = [1/2; ones(N-1,1); 1/2].*(-1).^(0:N)';
end
