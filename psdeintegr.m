function [sol, rhs_x] = psdeintegr(tau1, tau2, g, k1, f1, k2, f2, u, v, M, T, RelTol, AbsTol)
% PSDEINTEGR computes the solution of the IVP
%   x(t) = y(t) * \int_{-tau2}^{-tau1} k1(theta)f1(x(t+theta)) d theta, t in [0, T]
%   y'(t) = g(y(t)) + y(t) * \int_{-tau2}^{-tau1} k2(theta)f2(x(t+theta)) d theta, t in [0, T]
%   x(t) = u(t), t in [-tau2, 0]
%   y(t) = v(t), t in [-tau2, 0]
% via the pseudospectral collocation of the corresponding semilinear
% abstract differential equation using ODE45.
%
% [sol, rhs_x] = PSDEINTEGR(tau1, tau2, g, k1, f1, k2, f2, u, v, M, T, RelTol, AbsTol)
%
% The functions f, k1, k2, g1 and g2 must be vectorised.
% M is the degree of the interpolating polynomials.
% AbsTol and RelTol are the tolerances for ODE45.
% To reconstruct the solution of the renewal equation, apply rhs_x to the
% vector values of sol.
%
% References:
%   F. Scarabel, O. Diekmann, R. Vermiglio,
%   Numerical bifurcation analysis of renewal equations via pseudospectral approximation,
%   J. Comput. Appl. Math. 397, 113611 (2021),
%   DOI: 10.1016/j.cam.2021.113611
%
%   D. Breda, O. Diekmann, M. Gyllenberg, F. Scarabel, R. Vermiglio,
%   Pseudospectral discretization of nonlinear delay equations: New prospects for numerical bifurcation analysis,
%   SIAM J. Appl. Dyn. Syst. 15 (2016), pp. 1–23,
%   DOI: 10.1137/15M1040931
%
% psdeintegr.m, version 1.0, 2023-10-13
% Copyright (c) 2023 Davide Liessi
% This file is distributed under the terms of the MIT license (see `LICENSE.txt`).

% M >= 1
[x, bw, D] = cheb(M); % Chebyshev extrema in [-1, 1] and related quantities
% x, bw are column vectors
xx = (x-1)/2*tau2; % Chebyshev extrema in [-tau2, 0]
% xx is a column vector
DD = D*2/tau2; % differentiation matrix in [-tau2, 0]
% no need to rescale the barycentric weights: factors cancel out in formula
dDM = DD(2:end, :); % diff. mat. in [-tau2, 0] without 1st row
DM = DD(2:end, 2:end); % diff. mat. in [-tau2, 0] without 1st row & column

theta = (x-1)/2*(tau2-tau1)-tau1; % Chebyshev extrema in [-tau2, -tau1]
% theta is a column vector
ccw = clencurt(M);
% ccw is a row vector
% Clenshaw-Curtis weights need to be rescaled by a factor of (tau2-tau1)/2,
% see below.

ljprimetheta = NaN(M+1, M); % row: index in theta; column: j
for j = 1:M
    % Lagrange polynomials for Chebyshev extrema in [-tau2, 0]
    % @(t) barint(xx, bw, DD(:,j+1), t)
    ljprimetheta(:, j) = barint(xx, bw, DD(:,j+1), theta);
end
% derivative of state at theta (column vector, row: index in theta)
%derstate = @(state) ljprimetheta * state(1:M);
% apply nonlinear kernel
%integrand = @(state) k(theta) .* f(derstate(state));
% quadrature
%F = @(state) (tau2-tau1)/2 * ccw * integrand(state);

% statex = state(1:M);
% statey = state(M+1:2*M+1);
rhs_x = @(state) state(M+1) * (tau2-tau1)/2 * ccw * (k1(theta) .* f1(ljprimetheta * state(1:M)));
rhs_ode_x = @(state) DM * state(1:M) - rhs_x(state) * ones(M, 1);
rhs_ode_y = @(state) ...
    [g(state(M+1)) + state(M+1) * (tau2-tau1)/2 * ccw * (k2(theta) .* f2(ljprimetheta * state(1:M)));
    dDM * state(M+1:2*M+1)];

A = @(t, state) [rhs_ode_x(state); rhs_ode_y(state)];

integrated_state = DM \ u(xx(2:end));

sol = ode45(A, [0, T], [integrated_state; v(xx)], odeset('RelTol', RelTol, 'AbsTol', AbsTol));

end
