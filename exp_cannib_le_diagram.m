%% Diagram of Lyapunov exponents for the cannibalism RE varying gamma

% equation is y(t) = gamma/2*\int_{-3}^{-1}y(t+theta)*exp(-y(t+theta))d\theta
% use log(gamma) as continuation parameter
% trivial equilibrium 0 is stable for log(gamma) < 0
% nontrivial equilibrium log(gamma) is stable for 0 < log(gamma) < 1+pi/2
% periodic solution is stable for 1+pi/2 < gamma <\approx
% 3.79 [EJQTDE2016] or 3.88 [JCAM2021]
% C(t,theta) = gamma/2*exp(-\bar{y})*(1-\bar{y})

% Copyright (c) 2023 Davide Liessi
% This file is distributed under the terms of the MIT license (see `LICENSE.txt`).

%% Parameters

tau1 = 1;
tau2 = 3;

loggamma_start = 2;
loggamma_end = 4.83;
delta_loggamma = 0.01;

M = 15;
T = 1000;

TOL = 1e-6; % dqr
RelTol = 1e-6; % ode45
AbsTol = 1e-7; % ode45

loggamma = loggamma_start:delta_loggamma:loggamma_end;
[DM, lkprimetheta, scaledccwljprimetheta] = lrhs_nonlinearcoll_constants(tau1, tau2, M);

%% Compute, CL (collocation and then linearization)

le_all = cell(length(loggamma), 1);
le_t = cell(length(loggamma), 1);
le_clock = cell(length(loggamma), 1);
le_rejects = NaN(length(loggamma), 1);

for i = 1:length(loggamma)
    fprintf('log(gamma) = %d\n', loggamma(i))
    if loggamma(i) <= 0
        sol = psreintegr(1, 3, @(theta)theta*0+exp(loggamma(i))/2, @(y) y.*exp(-y), @(t) t*0, M, T, RelTol, AbsTol);
    elseif loggamma(i) <= 1+pi/2
        sol = psreintegr(1, 3, @(theta)theta*0+exp(loggamma(i))/2, @(y) y.*exp(-y), @(t) t*0+loggamma(i), M, T, RelTol, AbsTol);
    else
        initial_value = loggamma(i)+0.2;
        sol = psreintegr(1, 3, @(theta)theta*0+exp(loggamma(i))/2, @(y) y.*exp(-y), @(t) t*0+initial_value, M, T, RelTol, AbsTol);
    end
    A = @(t) lrhs_nonlinearcoll(t, @(y) exp(loggamma(i))/2 * exp(-y) .* (1 - y), sol, M, DM, lkprimetheta, scaledccwljprimetheta);
    % always reset the random number generator
    rng('default')
    [le_all{i}, le_t{i}, le_clock{i}, le_rejects(i)] = dqr(A, T, TOL);
end

%% Plot

% Due to the large amount of data it is better to split the variables in
% several MAT files: e.g. for each block of 100 values of gamma, start from
% clean variables, compute and save to a different MAT file.

le = cellfun(@(x) x(:,end), le_all, 'UniformOutput', false);

figure
plot(loggamma, cellfun(@(x) x(1), le), '.-', 'DisplayName', 'LE(1)')
hold on
plot(loggamma, cellfun(@(x) x(2), le), '.-', 'DisplayName', 'LE(2)')
grid on
title(sprintf('LE(1:2) for cannibalism RE with T=%d, M=%d, TOL=%d', T, M, TOL))
xlabel('log\gamma')
legend('Location', 'best')
