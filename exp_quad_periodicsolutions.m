%% Periodic solutions of the quadratic RE (to confirm the bifurcations)

% Test equation:
% y(t) = gamma/2*\int_{-3}^{-1}y(t+theta)*(1-y(t+theta))d\theta

% Copyright (c) 2023 Davide Liessi
% This file is distributed under the terms of the MIT license (see `LICENSE.txt`).

%% Parameters

tau1 = 1;
tau2 = 3;

gamma = [4, 4.4, 4.51, 4.87, 4.883, 4.887];

M = 15;
T = 1000;

TOL = 1e-6; % dqr
RelTol = 1e-6; % ode45
AbsTol = 1e-7; % ode45

% 1st branch of exact periodic solutions
solquad = @(t,gamma) 1/2+pi/4/gamma+sqrt(1/2-1/gamma-pi/2/gamma^2*(1+pi/4))*sin(pi/2*t);

%% Compute

sol = cell(length(gamma), 1);
G = cell(length(gamma), 1);

initial_value = 0.2;

for i = 1:length(gamma)
    [sol{i}, G{i}] = psreintegr(1, 3, @(theta)theta*0+gamma(i)/2, @(y) y.*(1-y), @(t) t*0+initial_value, M, T, RelTol, AbsTol);
end

%% Plot whole solutions

ttt = linspace(0, T, T*10);
solttt = NaN(1, length(ttt));

figure
for i = 1:length(gamma)
    subplot(2, 3, i)
    for j = 1:length(ttt)
        solttt(j) = G{i}(deval(sol{i}, ttt(j)));
    end
    plot(ttt, solttt);
end

%% Measured periods

% 1st branch
% 990.5614
% 994.5779 4.0165
% 998.5745 3.9966
% period 4

% 1st branch PD1
% (980.5961)
% 980.6011
% (988.6127)
% 988.6177 8.0166
% (996.6293)
% 996.6343 8.0166
% period 8.0166

% 1st branch PD2
% 960.356
% 976.4576 16.1066
% (976.4626)
% 992.5642 16.1066
% period 16.1066

% 2nd branch
% 985.6962
% (991.5578)
% 991.5628 5.8666
% 997.4243 5.8615
% period 5.86

% 2nd branch PD1
% 971.4611
% (983.2227)
% 983.2277 11.7666
% 994.9893 11.7616
% period 11.76
% 5.86*2=11.72

% 2nd branch PD2
% 933.9155
% 957.4471 23.5316
% 980.9737 23.5266
% period 23.53
% 5.86*4=23.44
% 11.76*2=23.52

periodapprox = [4, 8.0166, 16.1066, 5.86, 11.76, 23.53];

%% Plot a bit more than 3 periods with unit-based grid (to estimate periods)

nperiods = 3;
npointsperunit = 200;

figure
for i = 1:length(gamma)
    subplot(2, 3, i)
    timeunits = periodapprox(i)*nperiods+1;
    ttt = linspace(T-timeunits, T, npointsperunit*timeunits);
    solttt = NaN(1, length(ttt));
    for j = 1:length(ttt)
        solttt(j) = G{i}(deval(sol{i}, ttt(j)));
    end
    plot(ttt, solttt);
    hold on
    for k = 1:nperiods
        xline(T-k*periodapprox(i), 'r--');
    end
    xlabel('T')
    title(sprintf('\\gamma=%d, period=%d', gamma(i), periodapprox(i)))
    % if i==1
    %     plot(ttt, solquad(ttt-1.5596, gamma(i)), 'k')
    % end
end

%% Plot 2 periods with period-based grid

nperiods = 2;
npointsperperiod = [100, 100, 100, 100, 100, 200];

figure
for i = 1:length(gamma)
    subplot(2, 3, i)
    ttt = linspace(T-periodapprox(i)*nperiods, T, npointsperperiod(i)*nperiods);
    solttt = NaN(1, length(ttt));
    for j = 1:length(ttt)
        solttt(j) = G{i}(deval(sol{i}, ttt(j)));
    end
    plot(ttt, solttt);
    hold on
    for k = 1:(nperiods-1)
        xline(T-k*periodapprox(i), 'r--');
    end
    xlabel('T')
    title(sprintf('\\gamma=%d, period=%d', gamma(i), periodapprox(i)))
    % if i==1
    %     plot(ttt, solquad(ttt-1.5596, gamma(i)), 'k')
    % end
end
