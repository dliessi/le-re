%% Periodic solutions of the cannibalism RE (to confirm the bifurcations)

% equation is y(t) = gamma/2*\int_{-3}^{-1}y(t+theta)*exp(-y(t+theta))d\theta

% Copyright (c) 2023 Davide Liessi
% This file is distributed under the terms of the MIT license (see `LICENSE.txt`).

%% Parameters

tau1 = 1;
tau2 = 3;

loggamma = [3, 4.25, 4.6];

M = 15;
T = 1000;

TOL = 1e-6; % dqr
RelTol = 1e-6; % ode45
AbsTol = 1e-7; % ode45

%% Compute

sol = cell(length(loggamma), 1);
G = cell(length(loggamma), 1);

for i = 1:length(loggamma)
    initial_value = loggamma(i)+0.2;
    [sol{i}, G{i}] = psreintegr(1, 3, @(theta)theta*0+exp(loggamma(i))/2, @(y) y.*exp(-y), @(t) t*0+initial_value, M, T, RelTol, AbsTol);
end

%% Plot whole solutions

ttt = linspace(0, T, T*10);
solttt = NaN(1, length(ttt));

figure
for i = 1:length(loggamma)
    subplot(1, 3, i)
    for j = 1:length(ttt)
        solttt(j) = G{i}(deval(sol{i}, ttt(j)));
    end
    plot(ttt, solttt);
end

%% Measured periods

% 1st branch
% (990.3863)
% 990.3913
% 994.3878 3.9965
% (998.3844)
% 998.3894 4.0016
% period 4

% 1st branch PD1
% 977.9006
% 985.9672 8.0066
% 994.0338 8.0066
% period 8.0066

% 1st branch PD2
% 955.2154
% 971.5171 16.3017
% 987.8188 16.3017
% period 16.3017

periodapprox = [4, 8.0066, 16.3017];

%% Plot a bit more than 3 periods with unit-based grid (to estimate periods)

nperiods = 3;
npointsperunit = 200;

figure
for i = 1:length(loggamma)
    subplot(1, 3, i)
    timeunits = periodapprox(i)*nperiods+1;
    ttt = linspace(T-timeunits, T, npointsperunit*timeunits);
    solttt = NaN(1, length(ttt));
    for j = 1:length(ttt)
        solttt(j) = G{i}(deval(sol{i}, ttt(j)));
    end
    plot(ttt, solttt);
    hold on
    for k = 1:nperiods
        xline(T-k*periodapprox(i), 'r--');
    end
    xlabel('T')
    title(sprintf('log(\\gamma)=%d, period=%d', loggamma(i), periodapprox(i)))
end

%% Plot 2 periods with period-based grid

nperiods = 2;
npointsperperiod = [100, 100, 300];

figure
for i = 1:length(loggamma)
    subplot(1, 3, i)
    ttt = linspace(T-periodapprox(i)*nperiods, T, npointsperperiod(i)*nperiods);
    solttt = NaN(1, length(ttt));
    for j = 1:length(ttt)
        solttt(j) = G{i}(deval(sol{i}, ttt(j)));
    end
    plot(ttt, solttt);
    hold on
    for k = 1:(nperiods-1)
        xline(T-k*periodapprox(i), 'r--');
    end
    xlabel('T')
    title(sprintf('log(\\gamma)=%d, period=%d', loggamma(i), periodapprox(i)))
end
