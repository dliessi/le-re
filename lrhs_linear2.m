function A = lrhs_linear2(t, C, M, DM, theta, scaledccwljprimetheta)
% Right-hand side for the ODE discretising the linear RE
% x(t) = \int_{-\tau_2}^{-\tau_1} C(t,theta) x(t+theta) d\theta
%
% [DM, theta, scaledccwljprimetheta] = lrhs_linear2_constants(tau1, tau2, M)
% A = lrhs_linear2(t, C, M, DM, theta, scaledccwljprimetheta)
%
% The function C must be vectorised in theta.
% M is the degree of the interpolating polynomials.
%
% lrhs_linear2.m, version 1.0, 2023-10-13
% Copyright (c) 2023 Davide Liessi
% This file is distributed under the terms of the MIT license (see `LICENSE.txt`).

% M >= 1

% FF(1,j) = F(l_j'), j in 1:M
% Compute \int_{-tau2}^{-tau1}C(t,theta)l_j'(theta)dtheta as a row vector
% indexed by j in 1:M; theta is a row vector, so C(t,theta) is, too; see
% lrhs_linear2_constants.m for details.
LL = C(t,theta) * scaledccwljprimetheta;

% DM: differentiation matrix in [-tau2, 0] without first row and column
A = DM - repmat(LL, M, 1);
end
