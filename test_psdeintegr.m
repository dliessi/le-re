%% Tests for PSDEINTEGR (time integration of coupled RE and DDE via pseudospectral discretization)

% Test equation: simplified logistic Daphnia

% Copyright (c) 2023 Davide Liessi
% This file is distributed under the terms of the MIT license (see `LICENSE.txt`).

%% Parameters

abar = 3;
amax = 4;
r = 1;
K = 1;
g = @(y) r * y .* (1-y/K);
f1 = @(x) x;
gamma = 1;
k2 = @(theta) theta*0 - gamma;
f2 = @(x) x;
u = @(t) t*0+0.1;
v = @(t) t*0+0.1;
M = 10;
T = 1000;
RelTol = 1e-3;
AbsTol = 1e-4;

lll = linspace(0, T, T*10);

%%

beta = 0.5;
k1 = @(theta) theta*0 + beta;
[sol, rhs_x] = psdeintegr(abar, amax, g, k1, f1, k2, f2, u, v, M, T, RelTol, AbsTol);

figure
plot(lll, deval(sol, lll, M+1))
hold on
solval = deval(sol, lll);
myval = NaN(1, length(lll));
for i = 1:length(lll)
    myval(i) = rhs_x(solval(:, i));
end
plot(lll, myval)

%%

beta = 1.5;
k1 = @(theta) theta*0 + beta;
[sol, rhs_x] = psdeintegr(abar, amax, g, k1, f1, k2, f2, u, v, M, T, RelTol, AbsTol);

figure
plot(lll, deval(sol, lll, M+1))
hold on
solval = deval(sol, lll);
myval = NaN(1, length(lll));
for i = 1:length(lll)
    myval(i) = rhs_x(solval(:, i));
end
plot(lll, myval)

%%

beta = 2;
k1 = @(theta) theta*0 + beta;
[sol, rhs_x] = psdeintegr(abar, amax, g, k1, f1, k2, f2, u, v, M, T, RelTol, AbsTol);

figure
plot(lll, deval(sol, lll, M+1))
hold on
solval = deval(sol, lll);
myval = NaN(1, length(lll));
for i = 1:length(lll)
    myval(i) = rhs_x(solval(:, i));
end
plot(lll, myval)

%%

beta = 2.5;
k1 = @(theta) theta*0 + beta;
[sol, rhs_x] = psdeintegr(abar, amax, g, k1, f1, k2, f2, u, v, M, T, RelTol, AbsTol);

figure
plot(lll, deval(sol, lll, M+1))
hold on
solval = deval(sol, lll);
myval = NaN(1, length(lll));
for i = 1:length(lll)
    myval(i) = rhs_x(solval(:, i));
end
plot(lll, myval)
