function A = lrhs_nonlinearcoll(t, fprime, sol, M, DM, lkprimetheta, scaledccwljprimetheta)
% Right-hand side for the ODE discretising the RE
% x(t) = \int_{-\tau_2}^{-\tau_1} f(x(t+theta)) d\theta
% collocated and linearised around a solution sol.
%
% [DM, ljprimetheta, scaledccwljprimetheta] = lrhs_nonlinearcoll_constants(tau1, tau2, M)
% A = lrhs_nonlinearcoll(t, fprime, sol, M, DM, lkprimetheta, scaledccwljprimetheta)
%
% The function fprime is the derivative of f and must be vectorised.
% M is the degree of the interpolating polynomials.
%
% lrhs_nonlinearcoll.m, version 1.0, 2023-10-13
% Copyright (c) 2023 Davide Liessi
% This file is distributed under the terms of the MIT license (see `LICENSE.txt`).

% M >= 1

% FF(1,j) = \int_{-\tau_2}^{-\tau_1}
%            f'(\sum l_k'(theta)xbar_k(t)) l_j'(theta) d\theta, j in 1:M
% Compute f'(\sum l_k'(theta)xbar_k(t)) as a row vector indexed by j in
% 1:M; see lrhs_nonlinearcoll_constants.m for details.
fprimedersol = fprime(lkprimetheta * deval(sol, t));
% fprimedersol is a column vector of values corresponding to theta.
FF = fprimedersol' * scaledccwljprimetheta;

% DM: differentiation matrix in [-tau2, 0] without first row and column
A = DM - repmat(FF, M, 1);
end
