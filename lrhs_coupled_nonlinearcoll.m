function A = lrhs_coupled_nonlinearcoll(t, f1, f2, f1prime, f2prime, gprime, sol, M, DM, dDM, lkprimetheta, scaledccwljprimetheta, scaledccw)
% Right-hand side for the ODE discretising the coupled RE/DDE
%   x(t) = y(t) * \int_{-tau2}^{-tau1} f1(x(t+theta)) d theta
%   y'(t) = g(y(t)) + y(t) * \int_{-tau2}^{-tau1} f2(x(t+theta)) d theta
% collocated and linearised around a solution sol.
%
% [DM, dDM, ljprimetheta, scaledccwljprimetheta, scaledccw] = lrhs_coupled_nonlinearcoll_constants(tau1, tau2, M)
% A = lrhs_coupled_nonlinearcoll(t, f1, f2, f1prime, f2prime, gprime, sol, M, DM, dDM, lkprimetheta, scaledccwljprimetheta, scaledccw)
%
% The functions f1prime, f2prime and gprime are the derivatives of f1, f2 and g.
% The functions f1, f2, f1prime, f2prime must be vectorised.
% M is the degree of the interpolating polynomials.
%
% lrhs_coupled_nonlinearcoll.m, version 1.0, 2023-10-13
% Copyright (c) 2023 Davide Liessi
% This file is distributed under the terms of the MIT license (see `LICENSE.txt`).

% M >= 1

% FF(1,j) = \int_{-\tau_2}^{-\tau_1}
%            f'(\sum l_k'(theta)xbar_k(t)) l_j'(theta) d\theta, j in 1:M
% Compute f'(\sum l_k'(theta)xbar_k(t)) as a row vector indexed by j in
% 1:M; see lrhs_coupled_nonlinearcoll_constants.m for details.
solval = deval(sol, t);
f1primedersol = f1prime(lkprimetheta * solval(1:M));
f2primedersol = f2prime(lkprimetheta * solval(1:M));
% fprimedersol is a column vector of values corresponding to theta.
FF1 = f1primedersol' * scaledccwljprimetheta * solval(M+1);
FF2 = f2primedersol' * scaledccwljprimetheta * solval(M+1);

GG = gprime(solval(M+1));

% F(1,j) = \int_{-\tau_2}^{-\tau_1} f(\sum l_k'(theta)xbar_k(t)) d\theta
% Compute f(\sum l_k'(theta)xbar_k(t)) as a row vector indexed by j in
% 1:M; see lrhs_coupled_nonlinearcoll_constants.m for details.
f1dersol = f1(lkprimetheta * solval(1:M));
f2dersol = f2(lkprimetheta * solval(1:M));
F1 = f1dersol' * scaledccw;
F2 = f2dersol' * scaledccw;

% DM: differentiation matrix in [-tau2, 0] without first row and column
A = [DM - repmat(FF1, M, 1), -ones(M, 1) * F1, zeros(M);
    FF2, GG + F2, zeros(1, M);
    zeros(M), dDM];
end
