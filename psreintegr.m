function [sol, G] = psreintegr(tau1, tau2, k, g, u, M, T, RelTol, AbsTol)
% PSREINTEGR computes the solution of the IVP
%   y(t) = \int_{-tau2}^{-tau1} k(theta)g(y(t+theta)) d theta, t in [0, T]
%   y(t) = u(t), t in [-tau2, 0]
% via the pseudospectral collocation of the corresponding semilinear
% abstract differential equation using ODE45.
%
% [sol, G] = PSREINTEGR(tau1, tau2, k, g, u, M, T, RelTol, AbsTol)
%
% The functions k and g must be vectorised.
% M is the degree of the interpolating polynomials.
% AbsTol and RelTol are the tolerances for ODE45.
% To reconstruct the solution, apply G to the vector values of sol.
%
% Reference:
%   F. Scarabel, O. Diekmann, R. Vermiglio,
%   Numerical bifurcation analysis of renewal equations via pseudospectral approximation,
%   J. Comput. Appl. Math. 397, 113611 (2021),
%   DOI: 10.1016/j.cam.2021.113611
%
% psreintegr.m, version 1.0, 2023-10-13
% Copyright (c) 2023 Davide Liessi
% This file is distributed under the terms of the MIT license (see `LICENSE.txt`).

% M >= 1
[x, bw, D] = cheb(M); % Chebyshev extrema in [-1, 1] and related quantities
% x, bw are column vectors
xx = (x-1)/2*tau2; % Chebyshev extrema in [-tau2, 0]
% xx is a column vector
DD = D*2/tau2; % differentiation matrix in [-tau2, 0]
% no need to rescale the barycentric weights: factors cancel out in formula
DM = DD(2:end, 2:end); % diff. mat. in [-tau2, 0] without 1st row & column

theta = (x-1)/2*(tau2-tau1)-tau1; % Chebyshev extrema in [-tau2, -tau1]
% theta is a column vector
ccw = clencurt(M);
% ccw is a row vector
% Clenshaw-Curtis weights need to be rescaled by a factor of (tau2-tau1)/2,
% see below.

ljprimetheta = NaN(M+1, M); % row: index in theta; column: j
for j = 1:M
    % Lagrange polynomials for Chebyshev extrema in [-tau2, 0]
    % @(t) barint(xx, bw, DD(:,j+1), t)
    ljprimetheta(:, j) = barint(xx, bw, DD(:,j+1), theta);
end
% derivative of state at theta (column vector, row: index in theta)
%derstate = @(state) ljprimetheta * state;
% apply nonlinear kernel
%integrand = @(state) k(theta) .* g(derstate(state));
% quadrature
%G = @(state) (tau2-tau1)/2 * ccw * integrand(state);
G = @(state) (tau2-tau1)/2 * ccw * (k(theta) .* g(ljprimetheta * state));

A = @(t, state) DM * state - G(state) * ones(M, 1);

integrated_state = DM \ u(xx(2:end));

sol = ode45(A, [0, T], integrated_state, odeset('RelTol', RelTol, 'AbsTol', AbsTol));

end
