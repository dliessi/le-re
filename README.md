# LE-RE: Lyapunov exponents for renewal equations (also coupled with delay differential equations)

Approximation via discrete QR method [2, 3] of the Lyapunov exponents (LEs) of renewal equations (REs) and systems of coulped REs and delay differential equations (coupled equations) discretised as ODEs with [1, 4].

The method is implemented in the following files:
* `lrhs_linear2.m` and `lrhs_linear2_constants.m` for linear renewal equations;
* `lrhs_nonlinearcoll.m` and `lrhs_nonlinearcoll_constants.m` for nonlinear renewal equations;
* `lrhs_coupled_nonlinearcoll.m` and `lrhs_coupled_nonlinearcoll_constants.m` for nonlinear coupled equations.

If you use this software for a scientific publication, please cite the following publication:
> D. Breda, D. Liessi, A practical approach to computing Lyapunov exponents of renewal and delay equations, submitted, arXiv: [2310.15400](https://arxiv.org/abs/2310.15400) \[math.NA, math.DS\].

To reproduce the experiments therein, the following scripts are provided:
* `exp_quad_sol_compare.m`: comparison of the solutions of the RE with quadratic nonlinearity (5.1) computed with `tdqre.m` and `psreintegr.m` (Figure 1);
* `exp_quad_le_M_T_1000.m`: errors on the LEs of the quadratic RE (5.1) varying M and T (Figures 2 and 3);
* `exp_quad_le_diagram.m`: diagram of LEs of the quadratic RE (5.1) (Figure 4);
* `exp_quad_periodicsolutions.m`: some solutions of the quadratic RE (5.1) confirming the observed bifurcations (Figure 5);
* `exp_cannib_le_diagram.m`: diagram of LEs of the egg cannibalism RE (5.4) (Figure 6);
* `exp_cannib_periodicsolutions.m`: some solutions of the egg cannibalism RE (5.4) confirming the observed bifurcations (Figure 7);
* `exp_logdaphnia_le_diagram.m`: diagram of LEs of the simplified logistic Daphnia coupled equation (5.5) (Figure 8).

[1] D. Breda, O. Diekmann, M. Gyllenberg, F. Scarabel and R. Vermiglio,
Pseudospectral discretization of nonlinear delay equations: New prospects for numerical bifurcation analysis,
SIAM J. Appl. Dyn. Syst., 15 (2016), pp. 1–23,
DOI: [10.1137/15M1040931](https://doi.org/10.1137/15M1040931).

[2] L. Dieci and E. S. Van Vleck,
LESLIS and LESLIL: Codes for approximating Lyapunov exponents of linear systems,
2004,
URL: [https://dieci.math.gatech.edu/software-les.html](https://dieci.math.gatech.edu/software-les.html).

[3] L. Dieci, M. S. Jolly and E. S. Van Vleck,
Numerical techniques for approximating Lyapunov exponents and their implementation,
J. Comput. Nonlinear Dyn., 6:1 (2011), 011003,
DOI: [10.1115/1.4002088](https://doi.org/10.1115/1.4002088).

[4] F. Scarabel, O. Diekmann and R. Vermiglio,
Numerical bifurcation analysis of renewal equations via pseudospectral approximation,
J. Comput. Appl. Math., 397 (2021), 113611,
DOI: [10.1016/j.cam.2021.113611](https://doi.org/10.1016/j.cam.2021.113611).


## Copyright and licensing

### LE-RE (lrhs\_*.m, exp\_*.m)

Copyright (c) 2023 Davide Liessi

LE-RE, comprising the files `lrhs_*.m` and `exp_*.m`, is distributed under the terms of the MIT license (see `LICENSE.txt`).


### tdqre.m

Copyright (c) 2023 Davide Liessi

`tdqre.m` implements a restricted version of the method described in
E. Messina, E. Russo, A. Vecchio,
A stable numerical method for Volterra integral equations with discontinuous kernel,
J. Math. Anal. Appl. 337 (2008), pp. 1383–1393,
DOI: 10.1016/j.jmaa.2007.04.059.

`tdqre.m` and `test_tdqre.m` are distributed under the terms of the MIT license (see `LICENSE.txt`).


### psreintegr.m, psdeintegr.m

Copyright (c) 2023 Davide Liessi

References: [1, 4]

`psreintegr.m`, `psdeintegr.m`, `test_psreintegr.m` and `test_psdeintegr.m` are distributed under the terms of the MIT license (see `LICENSE.txt`).


### barint.m

Copyright (c) 2004 Jean-Paul Berrut, Lloyd N. Trefethen

The code in `barint.m` is taken, with few changes, from
J.-P. Berrut and L. N. Trefethen,
Barycentric Lagrange interpolation,
SIAM Review, 46(3):501-517, 2004,
DOI: 10.1137/S0036144502417715.

Even though the codes therein are not explicitly licensed, considering also that variations of them are included in [Chebfun](http://www.chebfun.org/), which is distributed under the 3-clause BSD license, I believe that the authors’ intention was for their codes to be freely used and that distributing these files together with MIT-licensed code does not violate their rights.


### cheb.m, clencurt.m

Copyright (c) 2000 Lloyd N. Trefethen

The code in `cheb.m` and `clencurt.m` is taken, with few changes in the case of cheb.m, from
L. N. Trefethen,
Spectral Methods in MATLAB,
SIAM, 2000,
DOI: 10.1137/1.9780898719598.

Even though the codes therein are not explicitly licensed, considering that they are made available for download on the author’s web page and that variations of some of them are included in [Chebfun](http://www.chebfun.org/), which is distributed under the 3-clause BSD license, I believe that the author’s intention was for his codes to be freely used and that distributing this file together with MIT-licensed code does not violate his rights.


### dqr.m

Copyright (c) 2018 Dimitri Breda, Sara Della Schiava

The code in `dqr.m` is taken from
D. Breda and S. Della Schiava,
Pseudospectral reduction to compute Lyapunov exponents of delay differential equations,
Discrete Contin. Dyn. Syst. Ser. B, 23(7):2727–2741, 2018,
DOI: 10.3934/dcdsb.2018092.

It is distributed here with permission by the authors.

A change was made to the original code in order to save and return the CPU time for each step and to return the number of rejected steps.
