function [Y, t, U, tpast] = tdqre(tau1, tau2, k, g, u, r2, T)
% TDQRE computes the solution of the IVP
%   y(t) = \int_{-tau2}^{-tau1} k(theta)g(y(t+theta)) d theta, t in [0, T]
%   y(t) = u(t), t in [-tau2, 0]
% with the trapezoidal direct quadrature method.
%
% [Y, t, U, tpast] = TDQRE(tau1, tau2, k, g, u, r2, T)
%
% Input
%   tau1, tau2: positive reals
%   k: function handle, domain [-tau2, -tau1], vectorised
%   g: function handle, domain R, vectorised
%   u: function handle, domain [-tau2, 0], vectorised
%   r2: positive integer, number of pieces in [-tau2, 0] for quadrature
%   T: positive real
%
% Output
%   Y: vector of values of the solution at t
%   t: vector of increasing times in [0, T]
%   U: vector of values of the solution at tpast
%   tpast: vector of decreasing times in [-tau2, 0]
%
% tdqre.m, version 1.0, 2023-10-13
% Copyright (c) 2023 Davide Liessi
% This file implements a restricted version of the method described in
%   E. Messina, E. Russo, A. Vecchio,
%   A stable numerical method for Volterra integral equations with discontinuous kernel,
%   J. Math. Anal. Appl. 337 (2008), pp. 1383-1393,
%   DOI: 10.1016/j.jmaa.2007.04.059.
% This file is distributed under the terms of the MIT license (see `LICENSE.txt`).

h = tau2/r2;
r1 = tau1/h;
N = ceil(T/h);
if abs((r1-ceil(r1))/r1) < eps
    r1 = ceil(r1);
elseif abs((r1-floor(r1))/r1) < eps
    r1 = floor(r1);
end
assert(floor(r1)==ceil(r1), 'tdqre:non_integer_r1', 'tau1 must be a multiple of tau2/r2')

tpast = h*(0:-1:-r2);
U = u(tpast); % U(n+1) = u(-n*h)
GU = g(U); % GU(n+1) = g(u(-n*h))
t = h*(0:N);
Y = NaN(1, N+1); % Y(n+1) = y(n*h)
GY = Y;
K = NaN(1, r2+1);
K(r1+1:r2+1) = k(-h*(r1:r2)); % K(n+1) = k(-n*h)

for n = 0:r1
    KGU = K(r1+1:r2+1).*GU((r1-n+1):(r2-n+1));
    KGU([1 end]) = KGU([1 end])/2;
    Y(n+1) = sum(KGU)*h;
    GY(n+1) = g(Y(n+1));
end

for n = r1+1:r2-1
    KGU = K(n+1:r2+1).*GU(1:(r2-n+1));
    KGU([1 end]) = KGU([1 end])/2;
    KGY = K(r1+1:n+1).*GY((n-r1+1):-1:1);
    KGY([1 end]) = KGY([1 end])/2;
    Y(n+1) = (sum(KGU)+sum(KGY))*h;
    GY(n+1) = g(Y(n+1));
end

for n = r2:N
    KGY = K(r1+1:r2+1).*GY((n-r1+1):-1:(n-r2+1));
    KGY([1 end]) = KGY([1 end])/2;
    Y(n+1) = sum(KGY)*h;
    GY(n+1) = g(Y(n+1));
end
end
