%% Tests for TDQRE (trapezoidal direct quadrature method for RE)

% Test equation:
% y(t) = gamma/2*\int_{-3}^{-1}y(t+theta)*(1-y(t+theta))d\theta
% trivial equilibrium 0 is stable for 0 < gamma < 1
% nontrivial equilibrium 1-1/gamma is stable for 1 < gamma < 2+pi/2
% periodic solution is stable for 2+pi/2 < gamma <\approx 4.32

% Copyright (c) 2023 Davide Liessi
% This file is distributed under the terms of the MIT license (see `LICENSE.txt`).

%% Parameters

tau1 = 1;
tau2 = 3;
k = @(theta,gamma) theta*0+gamma/2;
g = @(y) y.*(1-y);

r2 = 3*10;
T = 100;

% Some exact solutions
gamma_triveq = 0.5;
sol_triveq = @(t) t*0;
gamma_nontriveq = 3;
sol_nontriveq = @(t) t*0+1-1/gamma_nontriveq;
gamma_perexact = 4;
sol_perexact = @(t) 1/2+pi/4/gamma_perexact+sqrt(1/2-1/gamma_perexact-pi/2/gamma_perexact^2*(1+pi/4))*sin(pi/2*t);

%% Equilibria

[Y, t, U, tpast] = tdqre(tau1, tau2, @(theta) k(theta, gamma_triveq), g, sol_triveq, r2, T);
figure
plot(t, Y, 'DisplayName', sprintf('trivial eq. for \\gamma=%d, IV=eq.', gamma_triveq))
hold on
plot(tpast, U, 'DisplayName', sprintf('trivial eq. for \\gamma=%d, IV=eq.', gamma_triveq))

c = 0.2;
[Y, t, U, tpast] = tdqre(tau1, tau2, @(theta) k(theta, gamma_triveq), g, @(t) t*0+c, r2, T);
plot(t, Y, 'DisplayName', sprintf('trivial eq. for \\gamma=%d, IV=%d', gamma_triveq, c))
hold on
plot(tpast, U, 'DisplayName', sprintf('trivial eq. for \\gamma=%d, IV=%d', gamma_triveq, c))

[Y, t, U, tpast] = tdqre(tau1, tau2, @(theta) k(theta, gamma_nontriveq), g, sol_nontriveq, r2, T);
plot(t, Y, 'DisplayName', sprintf('nontrivial eq. for \\gamma=%d, IV=eq.', gamma_nontriveq))
hold on
plot(tpast, U, 'DisplayName', sprintf('nontrivial eq. for \\gamma=%d, IV=eq.', gamma_nontriveq))

c = 0.5;
[Y, t, U, tpast] = tdqre(tau1, tau2, @(theta) k(theta, gamma_nontriveq), g,  @(t) t*0+c, r2, T);
plot(t, Y, 'DisplayName', sprintf('nontrivial eq. for \\gamma=%d, IV=%d', gamma_nontriveq, c))
hold on
plot(tpast, U, 'DisplayName', sprintf('nontrivial eq. for \\gamma=%d, IV=%d', gamma_nontriveq, c))

xlabel('t')
title('Equilibria')
legend('Location', 'best')

%% Periodic solution

[Y, t, U, tpast] = tdqre(tau1, tau2, @(theta) k(theta, gamma_perexact), g, sol_perexact, r2, T);
figure
plot(t, Y, 'DisplayName', sprintf('per. sol. for \\gamma=%d, IV=per.sol.', gamma_perexact))
hold on
plot(tpast, U, 'DisplayName', sprintf('per. sol. for \\gamma=%d, IV=per.sol.', gamma_perexact))

c = 0.5;
[Y, t, U, tpast] = tdqre(tau1, tau2, @(theta) k(theta, gamma_perexact), g, @(t) t*0+c, r2, T);
plot(t, Y, 'DisplayName', sprintf('per. sol. for \\gamma=%d, IV=%d', gamma_perexact, c))
hold on
plot(tpast, U, 'DisplayName', sprintf('per. sol. for \\gamma=%d, IV=%d', gamma_perexact, c))

xlabel('t')
title('Periodic solution')
legend('Location', 'best')

%% Convergence of order 2

err = NaN(1,100);
for i=1:100
    [Y, t] = tdqre(tau1, tau2, @(theta) k(theta, gamma_perexact), g, sol_perexact, 3*i, T);
    err(i) = abs(Y(end)-sol_perexact(t(end)));
end
figure
loglog(3*(1:100), err, '.-', 'DisplayName', sprintf('absolute error at T=%d', T))
hold on
loglog(3*([1 100]), (3*[1 100]).^-2/5, 'DisplayName', 'order 2')
xlabel('r2')
title('Errors')
legend('Location', 'best')

%% Piecewise linear interpolation taking the discontinuity in 0 into account

c = 0.5;
[Y, t, U, tpast] = tdqre(tau1, tau2, @(theta) k(theta, gamma_perexact), g, @(t) t*0+c, r2, 3);

tt = linspace(-tau2, 3, 150);

% Naive interpolation without taking the discontinuity into account
vv_naive1 = interp1([tpast(end:-1:2), t], [U(end:-1:2), Y], tt, 'linear');
vv_naive2 = interp1([tpast(end:-1:1), t(2:end)], [U(end:-1:1), Y(2:end)], tt, 'linear');

% Correct interpolation taking the discontinuity into account
vv = NaN(size(tt));
vv(tt>=0) = interp1(t, Y, tt(tt>=0), 'linear');
vv(tt<0) = interp1(tpast(end:-1:1), U(end:-1:1), tt(tt<0), 'linear');

% This can be turned into a function: in that case, for performance
% reasons it is better to construct the interpolators externally (possibly
% with a helper function), as follows.
%
% giplus = griddedInterpolant(t, Y, 'linear', 'none');
% giminus = griddedInterpolant(tpast(end:-1:1), U(end:-1:1), 'linear', 'none');
%
% vv = NaN(size(tt));
% vv(tt>=0) = giplus(tt(tt>=0));
% vv(tt<0) = giminus(tt(tt<0));

figure
plot(t, Y, '.-', 'MarkerSize', 10, 'DisplayName', sprintf('%d nodes as output by TDQRE', length(t)))
hold on
plot(tpast, U, '.-', 'MarkerSize', 10, 'DisplayName', sprintf('r2+1=%d nodes as output by TDQRE', r2+1))
plot(tt, vv_naive1, 'x-', 'DisplayName', sprintf('naive pw linear interp. on %d uniform nodes (exclude 0 from [-tau2, 0])', length(tt)))
plot(tt, vv_naive2, '+-', 'DisplayName', sprintf('naive pw linear interp. on %d uniform nodes (exclude 0 from [0, T])', length(tt)))
plot(tt, vv, 'o-', 'DisplayName', sprintf('correct pw linear interp. on %d uniform nodes', length(tt)))
xlabel('t')
title(sprintf('Solution with \\gamma=%d, IV=%d', gamma_perexact, c))
legend('Location', 'best')
